import praw
import json
import os

#
# Script to download all your saved reddit entries.
#
# Requires praw https://praw.readthedocs.io/en/latest/getting_started/installation.html
#
# 1. Setup the environment variables (or just change them below in the code)
# REDDIT_CLIENT_ID
# REDDIT_CLIENT_SECRET
# REDDIT_USER_PASSWORD
#
# 2. Update the username
#  python script/saved.py

reddit = praw.Reddit(client_id=os.getenv("REDDIT_CLIENT_ID"),
                     client_secret=os.getenv("REDDIT_CLIENT_SECRET"),
                     password=os.getenv("REDDIT_USER_PASSWORD"),
                     user_agent="USERAGENT", username="rockerBOO")


def map_submission(item):
    submission = vars(item)

    print(submission.get('title'))

    return {
        'permalink': submission.get('permalink'),
        'selftext': submission.get('selftext'),
        'title': submission.get('title'),
        'name': submission.get('name'),
        'url': submission.get('url'),
        'id':  submission.get('id')
    }
    return submission


def get_saved(last="", results=[]):
    gen = reddit.user.me().saved(params={'after': last})

    i = 0

    for item in gen:
        results.append(item)
        last = item.name
        i = i + 1

    if i < 99:
        return results

    return get_saved(last, results)


submissions = list(map(map_submission, get_saved()))


f = open("saved.json", "w+")

json.dump(submissions, f)
print(len(submissions))
